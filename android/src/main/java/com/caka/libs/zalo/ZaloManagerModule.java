package com.caka.libs.zalo;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.zing.zalo.zalosdk.oauth.LoginVia;
import com.zing.zalo.zalosdk.oauth.OAuthCompleteListener;
import com.zing.zalo.zalosdk.oauth.OauthResponse;
import com.zing.zalo.zalosdk.oauth.ZaloOpenAPICallback;
import com.zing.zalo.zalosdk.oauth.ZaloSDK;

import org.json.JSONObject;

public class ZaloManagerModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public ZaloManagerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "ZaloManager";
    }

    @ReactMethod
    public void login(final Promise promise) {
        ZaloSDK.Instance.unauthenticate();
        final ReactApplicationContext activity = this.reactContext;
        final String[] Fields = {"id", "birthday", "gender", "picture", "name"};
        ZaloSDK.Instance.authenticate(getCurrentActivity(), LoginVia.APP_OR_WEB , new OAuthCompleteListener() {
            @Override
            public void onAuthenError(int errorCode, String message) {
                final String code = errorCode + "";
                promise.reject(code, message);
            }

            @Override
            public void onGetOAuthComplete(long uId, String oauthCode, String channel) {
                final WritableMap params = Arguments.createMap();
                params.putString("uId", "" + uId);
                params.putString("oauthCode", "" + oauthCode);
                params.putString("channel", "" + channel);
                ZaloSDK.Instance.getProfile(activity, new ZaloOpenAPICallback() {
                    @Override
                    public void onResult(JSONObject data) {
                        try {
                            WritableMap user = UtilService.convertJsonToMap(data);
                            params.putMap("user", user);
                            promise.resolve(params);
                        } catch (Exception ex) {
                            String message = ex.getMessage();
                            promise.reject("Get profile error", message);
                        }
                    }
                }, Fields);
            }

            @Override
            public void onGetOAuthComplete(OauthResponse oauthResponse) {
                super.onGetOAuthComplete(oauthResponse);
            }
        });
    }

    @ReactMethod
    public void logout() {
        ZaloSDK.Instance.unauthenticate();
    }


}
