import {
    NativeModules,
    Platform,
  } from 'react-native';
  
const { ZaloManager } = NativeModules;
  
  class Zalo {
    static async login() {
      if (Platform.OS === 'ios') {
        try {
          const oauthCode = await ZaloManager.login();
          return new Promise((resolve, reject) => {
            ZaloManager.getProfile((data) => {
              resolve({
                user: data,
                oauthCode,
                uId: null,
                channel: null,
              });
            }, (e) => {
              reject(e);
            });
          });
        } catch (error) {
          throw error;
        }
      } else {
        return await ZaloManager.login();
      }
    }
  
    static logout() {
      ZaloManager.logout();
    }
  }
  
export default Zalo;
  